<?php

class PashHash {
	
	private static $algo = '$2a';
	private static $cost = '$10';
	
	public static function unique_salt() {
		
		return substr(sha1(mt_rand()), 0, 22);
		
	}
	
	public static function hash($pass) {
		
		return crypt($pass, self::$algo . 
			self::$cost . 
			'$' . self::unique_salt());
		
	}
	
	public static function check_pass($hash, $pass) {
		
		$full_salt = substr($hash, 0, 29);
		$new_hash = crypt($pass, full_salt);
		return($hash == $new_hash);
		
	}
	
}

?>
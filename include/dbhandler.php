<?php

class DbHandler {
	
	private $conn;
	
	function __construct() {
		
		require_once dirname(__FILE__) . '/dbconnect.php';
		
		//opening db connection
		$db = new DbConnect();
		$this->conn = $db->connect();
	}
		
	//Helper For Creating New User
	public function createUser($student_id, $pass) {
		
		
		$response = array();
		
		if (!$this->isUserExists($student_id)) {
			
			$id = '';
			$hash = $this->hash($pass);
			$encryptPass = $hash["encrypt"];
			$salt = $hash["salt"];
			
			$query = "INSERT INTO m_login(id, student_number, passhash, salt) VALUES (?,?,?,?)";
			
			$stmt = $this->conn->prepare($query);
			$stmt->bind_param("ssss", $id, $student_id, $encryptPass, $salt);
			
			$result = $stmt->execute();
			$stmt->close();
			
			if($result) {
				return USER_CREATED_SUCCESSFULLY;
			}
			else {
				return USER_CREATE_FAILED;
			}
			
		}
		else {
			return USER_ALREADY_EXISTED;
		}
		
		return $response;
		
	}
	
	public function loginCheck($student_id, $pass) {
		
		$query = "SELECT	 passhash, salt 
						FROM m_login 
						WHERE student_number = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("s", $student_id);
		$stmt->execute();
			
		$stmt->bind_result($passhash, $salt);	
		$stmt->store_result();
		
		if($stmt->num_rows > 0) {
			
			$stmt->fetch();
			$hash = $this->checkHash($salt, $pass);
			$stmt->close();
			
			if($passhash == $hash) {
				return TRUE;
			}
			else {
				return FALSE;
			}
			
		}
		else {
			$stmt->close();
			return FALSE;
		}
		
	}
	
	public function isUserExists($student_id) {
		
		$query = "SELECT student_number
						FROM m_login
						WHERE student_number = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("s", $student_id);;
		$stmt->execute();
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->close();
		return $num_rows > 0;
		
	}
	
	public function hash($pass) {
		
		$salt = sha1(rand());
		$salt = substr($salt, 0, 10);
		$encrypt = base64_encode(sha1($pass . $salt, true) . $salt);
		$hash = array("salt" => $salt, "encrypt" => $encrypt);
		return $hash;
	} 
	
	
	public function checkHash($salt, $pass) {
		
		$hash = base64_encode(sha1($pass . $salt, true) . $salt);
        return $hash;
	}
	
	public function getStudentDetail($student_id) {
			
		$stmt = $this->conn->prepare("SELECT student_number, student_name FROM m_student WHERE student_number = ?");
		$stmt->bind_param("i", $student_id);
			
		if ($stmt->execute()) {
				
			$res = array();
			$stmt->bind_result($student_number, $student_name);
				
			$stmt->fetch();
			$res["student_number"] = $student_number;
			$res["student_name"] = $student_name;
			$stmt->close();
			return $res;
		}
		else {
			return NULL;
		}
			
	}
	
		
	public function getAllCourseResult($student_number) {
		
		$query = " SELECT distinct student_name, curriculum_subjectname, score_scoreconversion, curriculum_credit
						FROM m_student, m_score, m_lecturerworkload, m_curriculum, m_classmember
						WHERE student_number= ?
						AND m_score.score_lecturerworkload = m_lecturerworkload.lecturerworkload_id
						AND m_lecturerworkload.lecturerworkload_curriculum = m_curriculum.curriculum_id
						AND m_score.score_classmember=m_classmember.classmember_id
						AND m_classmember.classmember_student=m_student.student_id";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("i",$student_number);
		
		$stmt->execute();
		$tasks = $stmt->get_result();
		$stmt->close();
		return $tasks;
		
	}
	
	public function getCourseSmt($student_id, $smt) {
		$query = "SELECT curriculum_subjectname, score_scoreconversion, curriculum_credit
		FROM score_smt
		WHERE student_number = ? 
		AND curriculum_semester = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("ss", $student_id, $smt);
		$stmt->execute();
		$score = $stmt->get_result();
		$stmt->close();
		
		return $score;
	}
	
	public function getIps($student_id, $smt) {
		$query = 'SELECT round(sum(nilai)/sum(curriculum_credit), 2) as IPS 
						FROM score_smt 
						WHERE student_number= ? 
						AND curriculum_semester = ?';
		
		$stmt = $this->conn->prepare($query);
		$stmt->bind_param("ss", $student_id, $smt);
		
		if ($stmt->execute()) {
				
			$res = array();
			$stmt->bind_result($ips);
			$stmt->fetch();
			$response["IPS"] = $ips;
			$stmt->close();
			return $response;
		}
		else {
			return NULL;
		}
	}
	
	public function getIpk($student_id) {
		$query = 'SELECT round(sum(nilai)/sum(curriculum_credit), 2) as IPK 
						FROM score_smt 
						WHERE student_number= ? ';
		$stmt = $this->conn->prepare($query);
		
		$stmt->bind_param("s", $student_id);
		
		if ($stmt->execute()) {
				
			$res = array();
			$stmt->bind_result($ipk);
				
			$stmt->fetch();
			$response["IPK"] = $ipk;
			$stmt->close();
			return $response;
		}
		else {
			return NULL;
		}
	}
	
	public function getLectureSchadule() {
		$query = 'SELECT lecturer_name, hour_day, room_name, min(hour_starttime) as "from", max(hour_endtime) as "to"
						FROM m_lecturer, m_hour, m_room, m_lecturerworkload, m_programclass, m_curriculum, m_roomtype, m_academicyear, m_semester, m_hourgrup, m_schedule
						WHERE
						lecturerworkload_programclass=programclass_id and
						lecturerworkload_curriculum=curriculum_id and
						lecturerworkload_lecturer=lecturer_id and 
						room_type=roomtype_id and 
						hour_academicyear=academicyear_id and 
						hour_semester=semester_id and 
						hour_hourgroup=hourgroup_id and 
						schedule_lecturerworkload=lecturerworkload_id and 
						schedule_hour=hour_id and
						schedule_room=room_id
						GROUP BY lecturer_name, hour_day, room_name, hour_hourlabel, hour_starttime, hour_endtime
						ORDER BY hour_day ASC, lecturer_name ASC';
			
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$lectureSchadule = $stmt->get_result();
		$stmt->close();
		return $lectureSchadule;
	}
	
}


?>